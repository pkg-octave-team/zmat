Commands to build the package

```
VER=0.9.8
PKG=zmat
wget https://github.com/fangq/${PKG}/archive/v${VER}.tar.gz
tar zxvf v${VER}.tar.gz
# remove upstream binary files
rm -rf "${PKG}-${VER}/private" "${PKG}-${VER}/octave" v${VER}.tar.gz

cd ${PKG}-${VER}

# remove the bundled lz4 source code
rm -rf src/lz4
sed -i -e 's/lz4\/lz4 lz4\/lz4hc//g' src/Makefile
sed -i -e 's/-lz$/-lz -llz4/g' test/c/Makefile test/f90/Makefile
sed -i -e 's/include "lz4\/\(.*\)"/include <\1>/g' src/zmatlib.c
cd ..

# recreate the orig package
tar zcvf ${PKG}_${VER}.orig.tar.gz ${PKG}-${VER}
cd ${PKG}-${VER}

#download the debian packaging files
git clone https://salsa.debian.org/fangq/lib${PKG}.git debian

#build deb package
debuild -us -uc
```
