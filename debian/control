Source: zmat
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Qianqian Fang <fangqq@gmail.com>
Section: libs
Priority: optional
Standards-Version: 4.7.0
Build-Depends: debhelper-compat (= 13),
               dh-octave,
               libzstd-dev,
               liblz4-dev,
               libblosc2-dev
Homepage: https://github.com/NeuroJSON/zmat
Vcs-Git: https://salsa.debian.org/pkg-octave-team/zmat.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/zmat
Rules-Requires-Root: no

Package: libzmat1
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Provides: libzmat1
Description: compression library - runtime
 ZMat is a portable C library to enable easy-to-use data compression
 and decompression (such as zlib/gzip/lzma/lzip/lz4/lz4hc algorithms)
 and base64 encoding/decoding in an application.
 It is fast and compact, can process a large array within a fraction
 of a second. Among the supported compression methods, lz4 is the
 fastest for compression/decompression; lzma is the slowest but has
 the highest compression ratio; zlib/gzip have the best balance
 between speed and compression time.

Package: libzmat-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libzmat1 (= ${binary:Version}), ${misc:Depends}
Provides: libzmat-dev
Breaks: libzmat1-dev (<< 0.9.8+ds-4)
Replaces: libzmat1-dev
Description: compression library - development
 The libzmat1-dev package provides the headers files and tools you may need to
 develop applications using libzmat1.

Package: octave-zmat
Section: science
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${octave:Depends}, ${misc:Depends}
Description: in-memory data compression for Octave
 ZMat is a portable mex function to enable zlib/gzip/lzma/lzip/lz4/lz4hc
 based data compression/decompression and base64 encoding/decoding support
 in MATLAB and GNU Octave. It is fast and compact, can process a large
 array within a fraction of a second. Among the 6 supported compression
 methods, lz4 is the fastest for compression/decompression; lzma is the
 slowest but has the highest compression ratio; zlib/gzip have the best
 balance between speed and compression time.

Package: matlab-zmat
Section: contrib/science
Architecture: all
Depends: libzmat-dev (>= ${source:Version}), matlab-support, ${misc:Depends}
Description: in-memory data compression for MATLAB
 ZMat is a portable mex function to enable zlib/gzip/lzma/lzip/lz4/lz4hc
 based data compression/decompression and base64 encoding/decoding support
 in MATLAB and GNU Octave.
 .
 This package builds MATLAB bindings for zmat at installation
 time. Note that this package depends on MATLAB -- a commercial
 software that needs to be obtain and installed separately.
